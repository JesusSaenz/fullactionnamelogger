<?php

namespace Gamma\Events\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;

class Logger implements ObserverInterface
{

    protected $logger;

    public function __construct
    (
        LoggerInterface $logger
    )
    {
        $this->logger = $logger;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $fullActionName = $observer->getEvent()->getData('request')->getFullActionName();

        $this->logger->info(
            __('FullActionName Logger: %1', $fullActionName)
        );

        return;
    }
}