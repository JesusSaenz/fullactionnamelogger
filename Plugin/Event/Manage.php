<?php

namespace Gamma\Events\Plugin\Event;

use Psr\Log\LoggerInterface;
use Magento\Framework\Event\ManagerInterface;

class Manage
{
    private $logger;

    public function __construct
    (
          LoggerInterface $logger
    )
    {
          $this->logger = $logger;
    }
    
    public function beforeDispatch(ManagerInterface $subject, $event, array $data = [])
    {
	$this->logger->info(
	    __('Event dispatched: %1', $event)
	);
	return null;
    }
}

